const Util = require('./util');


module.exports = class Unit{
  constructor(){
    this.health = 1;
    this.recharged = 2000 +(Util.random(1,10)*20);
  }
  destroyed(){
    return this.health<=0;
  }

  successProbability(){
    return 0.5 * (1 + this.health/100) * Util.random(30 + this.experience, 100) / 100;
  }

  takesDamage(hit,isOperator=false){
    this.health -= hit;
    if(this.health<=0){
      if(!isOperator)console.log('Soldier killed!!!!!');
      this.health=0;
    }
    return true;
  }
  givesDamage(){
    return 0.05 + this.experience / 100;
  }
}
