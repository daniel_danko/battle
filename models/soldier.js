const Unit = require('./unit.js');
const Util = require('./util.js');

module.exports = class Soldier extends Unit{
  constructor(){
    super();
    this.experience = 0;
  }

  levelUp(){
    return this.experience++;
  }
}
