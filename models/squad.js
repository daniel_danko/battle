const Util = require('./util.js');
const Soldier = require('./soldier.js');
const Vehicle = require('./vehicle.js');
var type = [Soldier,Vehicle];


module.exports = class Squad{
  constructor(num=5,team,name){
    this.name = name
    this.team = team;
    this.unites = [];
    this.strategy = Util.strategy();
    this.recharge= this.recharged();
    for (var i = 0; i < num; i++) {
      this.unites.push(new type[Util.random(0,1)]());
    }
  }

  recharged(){
    var recharged=0;
    this.aliveUnites().map((unit)=>{
      if(recharged<unit.recharged) recharged=unit.recharged;
    })
    return recharged;
  }

  aliveUnites(){
    return  this.unites.filter((unit)=>{
      return unit.destroyed()==false;
    });
  }

  totalUnites(){
    return this.aliveUnites().length;
  }

  destroyed(){
    return this.aliveUnites().length<1;
  }

  successProbability(){
    var success = []
    this.aliveUnites().map((unit)=>{
     success.push(unit.successProbability());
    })
    return Util.gavg(success);
  }

  totalHealth(){
    let health=0;
    this.aliveUnites().map((unit)=>{
      health+= (unit.constructor.name == 'Vehicle') ? unit.totalHealth() : unit.health;
    })
    return health;
  }

  totalExperience(){
    let experience=0;
    this.aliveUnites().map((unit)=>{

      experience+= (unit.constructor.name == 'Vehicle')? Util.sum(this.operatorsExperience()):unit.experience;
    })
    return experience;
  }

  takesDamage(damage){
    var  damagePerUnit=damage/this.aliveUnites().length;
    this.aliveUnites().map((unit)=>{
      unit.takesDamage(damagePerUnit);
    })
  }

  givesDamage(){
    let allDamage = 0;
    this.aliveUnites().map((unit)=>{
     allDamage+=unit.givesDamage();
    })
    return allDamage;
  }

  levelUp(){
    return this.aliveUnites().map((unites)=>{
      return unites.levelUp();
    });
  }

}
