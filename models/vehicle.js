const Unit = require('./unit.js');
const Soldier = require('./soldier.js');
const Util = require('./util.js');

module.exports = class Vehicle extends Unit{
  constructor(num=3){
    super();
    this.recharged-=1000;
    this.operators = [];
    for(var i=0;i<num; i++){
      this.operators.push(new Soldier());
    }
  }

  aliveOperators(){
    return  this.operators.filter((operator)=>{
              return operator.destroyed()===false;
            })
  }
  destroyed(){
     if (this.health<=0 || this.aliveOperators().length<1) return true;
     return false;
  }
  totalHealth(){
    if(this.destroyed()) return 0;
    return this.health + this.averigeOpHealth();
  }

  averigeOpHealth(){
    var health=0;
    this.aliveOperators().map((operator)=>{
     health+=operator.health;

    })
    return Math.floor(health/this.operators.length);
  }

  operatorsExperience(){
    var experience=[];
    this.aliveOperators().map((unit)=>{
       experience.push(unit.experience);
    })
    return experience;
  }

  operatorsSuccess(){
    var success=[];
    this.aliveOperators().map((operator)=>{
       success.push(operator.successProbability());
    })
    return success;
  }

  successProbability(){
    return 0.5 * (1 + this.health/100) * Util.gavg(this.operatorsSuccess());
  }

  givesDamage(){
    return 0.1 + (Util.sum(this.operatorsExperience()) / 100)
  }

  takesDamage(hit){
    this.health=this.health-((hit*3)/10);
    var arr = this.aliveOperators();
    var random = Util.random(0,arr.length-1)
    var operator1 = arr[random];
    operator1.takesDamage(hit/2,true);
    arr.slice(random,1);
    var restOfHit=(hit*2)/10;
     if(arr.length==0){
       this.health= this.health-restOfHit
     }
     else{
       arr.map((operator)=>{
         operator.takesDamage(restOfHit/arr.length,true);
       })
     }
     if(this.health<=0 || this.aliveOperators().length<1){
       console.log('Vehicle destroyed!');
       this.health=0;
     }
  }
  
  levelUp(){
    return this.aliveOperators().map((operator)=>{
      return operator.levelUp();
    })
  }
}
