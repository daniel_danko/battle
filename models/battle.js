const Util = require('./util');
const Army = require('./army');


module.exports = class Battle{
  constructor(num=3){
    this.armies = [];
    for (var i = 1; i <= num; i++) {
      this.armies.push(new Army(3,i));
    }
  }

  start(){
    let left;
      this.armies.map((army)=>{
          army.activeSquads().map((squad)=>{
            this.attack(squad);
          });
      })
  }

  exchange(attacker,defender){
    if(attacker.successProbability()>defender.successProbability()){
      console.log(`Attack by squad ${attacker.name} was success!`);
      defender.takesDamage(attacker.givesDamage());
      console.log(`Damage delth is ${attacker.givesDamage()} to team ${defender.team} squad ${defender.name}!`);
      attacker.levelUp();
      return true
    }
    else console.log(`Attack by squad ${attacker.name} was blocked by team ${defender.team} squad ${defender.name}!`);
  }

  allEnemieSquads(team){
    var squads = [];
    this.armies.map((army)=>{
      if(army.team!=team && !army.destroyed()){
        army.activeSquads().map((squad)=>{
          squads.push(squad);
        })
      }
    })
    return squads;
  }

  findTarget(arr,strategy='strongest'){
    arr=this.targetByCriteria(arr,'health',strategy)
    if(arr.length==1) return arr[0];
    arr=this.targetByCriteria(arr,'experience',strategy)
    if(arr.length==1) return arr[0];
    arr=this.targetByCriteria(arr,'unites',strategy);
    if(arr.length==1) return arr[0];
    arr=this.targetByCriteria(arr,'damage',strategy);
    return arr[0];
  }

  targetByCriteria(arr,criteria,strategy){
    var value = '';
    let stored=0;
    let index;
    arr.forEach((squad,key)=>{

      switch (criteria) {
        case 'health':
          value = squad.totalHealth();
          break;
        case 'experinece':
          value = squad.totalExperience();
          break;
        case 'unites':
          value = squad.totalUnites;
          break;
        case 'damage':
          value = squad.givesDamage();
          break;
        default:
          value = squad.givesDamage();
      }
      if(strategy=='weakest'){
        if(stored>value){
          if(key!=0)  arr.splice(index, 1);
          stored=value;
          index=key;
        }
        else if(stored<value){
          arr.splice(key, 1);
        }
      }
      else if(strategy=='strongest'){
        if(stored<value){
          if(key!=0)  arr.splice(index, 1);
          stored=value;
          index=key;
        }
        else if(stored>value){
          arr.splice(key, 1);
        }
      }
    });
    return arr;
  }

  useStrategy(strategy){
    switch (strategy) {
      case 'random':
        return this.allEnemieSquads[Util.random(0,this.allEnemieSquads.length-1)];
        break;
      case 'weakest':
        return findTarget(this.allEnemieSquads,'weakest');
        break;
      case 'strongest':
        return findTarget(this.allEnemieSquads);
          break;
      default:

    }
  }
  attack(squad){
    var timeout = setTimeout(()=>{
      if (squad.destroyed()) {
        clearTimeout(timeout);
        return
      }
      console.log('********************************************');
      console.log(`Team ${squad.team} attackes!`);
      var target = this.findTarget(this.allEnemieSquads(squad.team),squad.strategy);
      if(!target){
        console.log('Army '+squad.team+'WON!');
        return
      }
      else{
        this.exchange(squad,target);
        if(target.destroyed()) console.log(`Squad ${target.name} of team ${target.team} was destoyed!!!!!`);
        var targetArmy = this.armies.filter((army)=> army.team == target.team);
        if(targetArmy[0].destroyed()) console.log(`Army ${targetArmy[0].team} destroyed!!!!!`);
        this.attack(squad);
      }


    },squad.recharged());

  }
}
