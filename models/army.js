const Squad = require('./squad.js');
const sqname = ['alpha','beta','gama']
module.exports = class Army{
  constructor(num=2,team){
    this.squads = []
    this.team = team
    for (var i = 0; i < num; i++) {
      this.squads.push(new Squad(5,this.team,sqname[i]));
    }
  }
  activeSquads(){
    return this.squads.filter((squad)=>{
      return squad.destroyed()==false;
    })
  }

  destroyed(){
    return this.activeSquads().length<1

  }


}
